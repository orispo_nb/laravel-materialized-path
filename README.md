# DEPRECATED (заброшено): 

**код не подходит для работы с нашей структурой, либо же непонятная структура, подлежит переписыванию на 90%**

cloned by [langaner/materialized-path](https://github.com/langaner/materialized-path)

### Installation

1) Add next code to `composer.json`.

```
    "repositories": [
        {
            "url": "https://bitbucket.org/orispo_nb/laravel-materialized-path.git",
            "type": "git"
        }
    ],
```

2)Run `composer require orispo_nb/laravel-materialized-path` to pull down the latest version of the package.

3)Now open up app/config/app.php and add the service provider to your providers array.

```
Nlrs\MaterializedPath\MaterializedPathServiceProvider::class,
```

4)Add the trait to model what need tree implementation

```
use \Nlrs\MaterializedPath\MaterializedPathTrait;
```

### Migrations

Table migration example `create_pages_table`:

```php
    Schema::create('pages', function(Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->bigIncrements('id');

        // parent id
        $table->bigInteger('parent_id')->unsigned()->nullable();
        // depth path consisting of models id
        $table->string('path');
        // depth path consisting of models alias
        $table->string('real_path')->unique();
        // alias field
        $table->string('alias')->unique();
        // order position
        $table->integer('position')->default(0)->index();
        // depth level
        $table->integer('level')->default(0)->index();

        $table->foreign('parent_id')->references('id')->on('pages')->onDelete('cascade');
    });
```

### Available methods

```php
    // Make model is root
    with(new Page())->makeRoot();

    // Make model first children by parent id
    with(new Page())->makeFirstChildOf($parentId);

    // Make model last children by parent id
    with(new Page())->makeLastChildOf($parentId);

    // Make previous sibling
    $page = Page::find(2);
    $page->makePreviousSiblingOf(Page::find(1));

    // Make next sibling
    $page = Page::find(2);
    $page->makeNextSiblingOf(Page::find(1));

    // Update model data
    $page = Page::find(1);
    $page->position = 2;
    with(new Page())->updateNode($page);

    // Get parent
    Page::find(1)->parent()->get();
    
    // Get sibling
    Page::find(1)->sibling()->get();

    // Get childrens by depth
    Page::find(1)->childrenByDepth()->get();

    // Get parents by depth
    Page::find(1)->parentByDepth()->get();

    // Descendant check
    Page::find(1)->isDescendantOf(Page::find(2));

    // Ancestor check
    Page::find(1)->isAncestorOf(Page::find(2));

    // Is leaf
    Page::find(1)->isLeaf();

    // All leafs of model
    Page::allLeaf();

    // All roots of model
    Page::allRoot();

    // Get exploded path
    Page::find(1)->getExplodedPath();

    // Build real path by entities
    with(new Page())->buildRealPath($ids);

    // Build model tree
    Page::find(1)->buildTree();

    // Build model tree by parent id
    Page::find(1)->buildChidrenTree();
```

### Configuration

Publish assets `php artisan vendor:publish`

This command initialize config file, located under app/config/packages/langaner/materialized-path/materialized_path.php.

Set `with_translations` to `true` if you need translation in result model. This option required translation pack `https://github.com/dimsav/laravel-translatable`
